#!/bin/sh

PROJECT_ROOT="${PWD}"
NOW=$(date +%Y%m%d%H%M%S)
SCRIPT_NAME=$(basename "$0")

declare -a USERS=(
    "ctrlc:" # USERNAME:PASSWORD
)

declare -a PACKAGES=(
#--- DEVELOPMENT
    "git"
    "base-devel"
    "linux-headers"
#--- EDITORS
    "nano"
    "nano-syntax-highlighting"
#--- FILESYSTEM
    "xfsprogs"
    "ntfs-3g"
#--- UTILITIES
    "tree"
    "mc"
    "most"
    "htop"
    "ncdu"
    "scrot"
    "tmux"
    "python-pywal"
#--- NETWORK
    "whois"
    "net-tools"
    "ufw"
    "aria2"
    "vnstat"
#--- DECOMPRESSION
    "unzip"
    "p7zip"
    "unrar"
    "unarj"
    "unace"
#--- SOUND
    "bluez-libs"
    "pulseaudio"
    "pulseaudio-alsa"
    "pulseaudio-bluetooth"
    "pulsemixer"
    "openal"
#--- SHELLS
    "fish"
#--- USABILITY
    "festival"
    "festival-us"
#--- XORG
    "xorg" 
    "xbindkeys" 
    "xorg-xdm" 
    "xclip" 
    "xsel"
#--- OPENBOX
    "openbox"
    "python-pyxdg" 
    "conky" 
    "dunst" 
    "libnotify" 
    "python-pyinotify" 
    "rofi" 
    "wmctrl" 
    "transset-df" 
    "xcompmgr" 
    "devilspie"
    "gvfs-smb"
    "mate-polkit" 
    "gsimplecal" 
#--- DESKTOP
    "firefox"
    "profile-sync-daemon" 
    "sakura"
    "caja"
    "caja-open-terminal" 
    "mate-calc" 
    "eom" 
    "engrampa" 
    "atril" 
    "redshift" 
    "gufw" 
    "xed" 
    "qt5ct" 
    "transmission-gtk" 
    "ttf-roboto-mono" 
#--- 3D
    "nvidia"
#--- WINE
    "wine" 
    "samba"
    "wine-gecko" 
    "wine-mono" 
    "winetricks" 
    "lib32-libpng" 
    "lib32-giflib" 
    "lib32-gnutls" 
    "lib32-nvidia-utils" 
    "lib32-gstreamer" 
    "lib32-gst-plugins-good" 
#--- MULTIMEDIA
    "mpv" 
    "youtube-dl" 
    "qmmp" 
    "projectm" 
    "ffmpegthumbnailer" 
    "brasero" 
    "libdvdcss" 
    "gimp"
#--- VIRTUALIZATION
    "virtualbox-host-modules-arch"
    "virtualbox" 
    "virtualbox-guest-iso" 
#--- GAMES
    "retroarch" 
    "steam"
)

declare -a ABS=(
#--- OPENBOX
    "tint3-cpp-git"
    "fittstool"
    "netwmpager"
    "networkmanager-dmenu-git"
    "ttf-zekton-rg"
    "xwinwrap-git"
    "rofi-greenclip"
    "skippy-xd-git"
    "ttf-ms-fonts"
    "pavolume-git"
    "xscreensaver-backends"
    "picom-tryone-git"
#--- DRIVERS
    "aic94xx-firmware"
    "wd719x-firmware"
#--- COMMS
    "teams"
    "gfeeds-git"
#--- VIRTUALIZATION
    "virtualbox-ext-oracle"
)

function check_previleges () {
    printf "\nValidating run previleges... user "$(whoami)" ("$EUID")\n"
    if [ "$EUID" -eq 0 ]
      then printf "Please run as a regular user to continue...\n\n"
      exit
    fi
}

function update_system () {
    printf "\nUpdating system...\n"
    sudo pacman -Suyq --color=always
}

function process_pacman () {
    printf "\nInstalling Pacman packages...\n"
    for PACKAGE in "${PACKAGES[@]}"; do
        printf "$PACKAGE\n"
        sudo pacman -S --color=always --noconfirm "$PACKAGE"
    done
}

function install_yay () {
    printf "\nInstalling yay...\n"
    cd "${PROJECT_ROOT}"
    git clone https://aur.archlinux.org/yay 
    cd yay
    makepkg -sirc --noconfirm
    cd "${PROJECT_ROOT}"
}

function process_abs () {
    printf "\nInstalling ABS (yay) packages...\n"
    for PACKAGE in "${ABS[@]}"; do
        printf "$PACKAGE\n"
        yay -S --noconfirm "$PACKAGE"
    done
}

# --- TROUBLE AHEAD ---------------------------------------------------------------------------
#
#

function configure_system () {
    printf "\nConfiguring system...\n"
#--- Services 
    sudo systemctl enable xdm
    sudo systemctl enable bluetooth
    sudo systemctl enable ufw
    sudo systemctl enable vnstat
    sudo systemctl restart vnstat
#--- Misc
#--- TODO: vnstat could be better
    sudo vnstat --add -i `ip -o link show | awk '{print $2,$9}' | grep UP | awk '{print $1}' | sed 's/://'`
#--- Fonts
    sudo ln -s /etc/fonts/conf.avail/11-lcdfilter-default.conf /etc/fonts/conf.d
    sudo ln -s /etc/fonts/conf.avail/10-sub-pixel-rgb.conf /etc/fonts/conf.d
#--- Copy system setting files
    sudo cp -r "${PROJECT_ROOT}"/boot/* /boot/
    sudo cp -r "${PROJECT_ROOT}"/etc/* /etc/
#--- Regen init
    sudo mkinitcpio -p linux
}

function configure_user () {
    printf "\nConfiguring user accounts...\n"
    for USER in "${USERS[@]}"; do
#--- Get User details
       USER_NAME="$( cut -d ':' -f 1 <<< "$USER" )"
       USER_PASSWORD="$( cut -d ':' -f 2- <<< "$USER" )"
       printf "User name: "$USER_NAME"\n"
       printf "User password: "$USER_PASSWORD"\n"
#--- User has already been created
       #sudo useradd -m -d /home/"$USER_NAME" -p `openssl passwd -crypt "$USER_PASSWORD"` "$USER_NAME" -s /bin/bash
#--- Dotfiles
       printf "Configuring "$USER_NAME" account\n"
       su -c "git clone https://gitlab.com/antonio.godinho/dotfiles.git ~/dotfiles" "$USER_NAME"
#--- Hack: yay saves files into .config
       su -c "rm -rf ~/.config" "$USER_NAME"
       su -c "mv ~/dotfiles/.* -vf ~/" "$USER_NAME"
       su -c "rm -rf ~/dotfiles" "$USER_NAME"
#--- Add to groups
       sudo gpasswd -a "$USER_NAME" vboxusers
#--- User services
       su -c "systemctl --user enable psd" "$USER_NAME"
#--- Misc
       su -c "wal --theme sexy-neon -q" "$USER_NAME"
       su -c "mkdir ~/.wine" "$USER_NAME"
#--- SSH (TODO: Not tested)
       su -c "eval (ssh-agent -c)" "$USER_NAME"
       su -c "ssh-add /backup/.keys/"$USER_NAME"_gitlab_com_ed25519" "$USER_NAME"
    done
}

function cleanup () {
    printf "\nCleaning up the system...\n"
    sudo pacman -Rus --color=always --noconfirm vim
    rm -rf "${PROJECT_ROOT}"/yay
    yay -Yc --noconfirm
}

printf "\n${SCRIPT_NAME} version 20200612 written by Antonio Godinho (ctrlc@tutanota.com)\n"
check_previleges
update_system
process_pacman
install_yay
process_abs
configure_system
configure_user
cleanup
printf "\n /// FINISH ${SCRIPT_NAME} ///\n\n"
while true; do
    read -p "Do you wish to reboot the system? " yn
    case $yn in
        [Yy]* ) reboot; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer [Y] yes or [N] no.";;
    esac
done

