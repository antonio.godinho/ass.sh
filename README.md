# ass.sh

**A.S.S. (Automated System Setup)**
An automated archlinux setup shell script I use to quickly setup **my** system after installation.

**Run at your own risk!**
And if you do... before running the script do edit: "USERNAME_CHANGE_ME:PASSWORD_CHANGE_ME" to the username and password of your liking. You might want to change the PACKAGES array which contains the list of packages to install and the ABS array which contains a list of software to download & compile from the ABS. 

Further editing can be done after "TROUBLE AHEAD" to match your system/user configurations. 

